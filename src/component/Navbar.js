import { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
const NavbarMenu = () => {
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    window.onscroll = function (e) {
      setScrolled(true);
    };
  }, []);
  return (
    <>
      <Navbar
        style={{
          background: "#FFFFFF",
          position: "fixed",
        }}
        expand="sm"
        className={scrolled ? "scrolled auto" : "auto"}
      >
        <Container>
          <Navbar.Brand href="#">
            <b style={{ color: "#061283" }}>RISKI</b>
            <b style={{ color: "#061283" }}>DN</b>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-3 my-lg-0  "
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <Nav.Link href="#home" className="font-weight">
                <b>Home</b>
              </Nav.Link>
              <Nav.Link href="#about" className="font-weight">
                <b>About</b>
              </Nav.Link>
              <Nav.Link href="#portfolio" className="font-weight">
                <b>Portfolio</b>
              </Nav.Link>

              <Nav.Link href="#contact" className="font-weight">
                Contact Me
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default NavbarMenu;
