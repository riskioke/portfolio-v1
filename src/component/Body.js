import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/esm/Button";
import personal from "../image/profil.png";
import Carousel from "react-bootstrap/Carousel";
import painting1 from "../image/painting1.jpg";
import painting2 from "../image/painting2.jpg";
import painting3 from "../image/painting3.jpg";

import game1 from "../image/game1.jpg";
import game2 from "../image/game2.jpg";
import game3 from "../image/game3.jpg";

import gadget1 from "../image/gadgeout1.jpg";
import gadget3 from "../image/gadgeout2.jpg";
import gadget2 from "../image/gadgeout3.jpg";

import catering1 from "../image/catering1.jpg";
import catering2 from "../image/catering2.jpg";
import catering3 from "../image/catering3.jpg";

import Card from "react-bootstrap/Card";

import AOS from "aos";
import "aos/dist/aos.css";
AOS.init();

const Body = () => {
  return (
    <>
      <section style={{ background: "#FFFFFF" }} id="home">
        <Container
          style={{
            minHeight: "100vh",
            background: "#FFFFFF",
          }}
        >
          <Row id="flex-column">
            <Col md="6" data-aos="fade-down">
              <div id="title">Riskidn.</div>
              <div>
                <b className="hero-font">
                  I am an employee with three years of work experience in
                  quality assurance. I worked for a company in the automotive
                  industry. Now, I am interested in working as a web developer,
                  and Im trying to reach that goal.
                </b>
              </div>
              <Button className="home-button" href="#contact">
                Contact Me
              </Button>
            </Col>
            <Col md="6" className="p-3 " data-aos="fade-down">
              <div className="image">
                <img
                  src={personal}
                  alt="personal"
                  width="250"
                  style={{ border: "10px dashed #061283" }}
                ></img>
              </div>
            </Col>
          </Row>
        </Container>{" "}
      </section>
      <div
        style={{
          position: "fixed",
          right: "20px",
          bottom: "70px",
          zIndex: "999999999",
        }}
      >
        <a href="https://wa.me/+6282322878769">
          <button
            style={{
              background: " rgba(100, 100, 100, 0)",
              verticalAlign: "center",
              height: "auto",
              border: "none",
              // borderRadius: "5px",
            }}
          >
            <img
              width="60"
              src="https://png.pngtree.com/png-vector/20221018/ourmid/pngtree-whatsapp-mobile-software-icon-png-image_6315991.png"
              alt="wa"
            ></img>{" "}
          </button>
        </a>
      </div>
      <section style={{ background: "#E1EDFB" }} id="about">
        <Container
          style={{
            minHeight: "80vh",
            background: "#E1EDFB",
            textAlign: "center",
          }}
        >
          {" "}
          <Row className="row-about">
            <Col
              style={{ margin: "auto", textAlign: "left" }}
              data-aos="fade-up"
            >
              {" "}
              <h5 className="font-color ">About Me</h5>
              <h1 className="hero-font margin-right">Riski Dwi Nugroho</h1>
              <p className="hero-font margin-right">
                I am interested in working as a web developer.
              </p>
              <p></p>
            </Col>
            <Col
              style={{ margin: "auto", textAlign: "left" }}
              data-aos="fade-up"
            >
              {" "}
              <br></br>
              <h1 className="hero-font">Social Media</h1>
              <p>Haii 🖐, You can contact me here.</p>
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                <a
                  href="https://www.instagram.com/panjeks_"
                  className="rounded"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="50"
                    height="50"
                    fill="#061283"
                    class="bi bi-instagram"
                    viewBox="0 0 20 20"
                  >
                    <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z" />
                  </svg>
                </a>

                <a
                  href="https://www.linkedin.com/in/riski-dwi-nugroho-8b8b65222/"
                  className="rounded"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="50"
                    height="50"
                    fill="#061283"
                    class="bi bi-linkedin"
                    viewBox="0 0 20 20"
                  >
                    <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                  </svg>
                </a>

                <a
                  href="https://web.facebook.com/panji.laras.5623293/"
                  className="rounded"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="50"
                    height="50"
                    fill="#061283"
                    class="bi bi-facebook"
                    viewBox="0 0 20 20"
                  >
                    <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z" />
                  </svg>
                </a>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <section style={{ background: "#FFFF" }} id="portfolio">
        <Container
          style={{
            minHeight: "100vh",
            background: "#ffff",
            textAlign: "center",
            paddingTop: "20vh",
          }}
        >
          <h5 data-aos="fade-down">Portfolio</h5>
          <h2 data-aos="fade-down" style={{ marginBottom: "10px" }}>
            Latest Project
          </h2>
          <div className="display-flex">
            {/* Lukisan */}
            <div className="div-container" data-aos="fade-down">
              <Carousel
                variant="dark"
                interval={1000}
                prevIcon={null}
                nextIcon={null}
              >
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius"
                    src={painting1}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius"
                    src={painting2}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius"
                    src={painting3}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>
              <Card style={{ border: "none" }}>
                <Card.Body>
                  <Card.Title>Artikel Lukisan</Card.Title>
                  <Card.Text>
                    Ini adalah project pertama yang dibuat dengan basic html,
                    css dan javascript
                  </Card.Text>
                  <Button
                    href="https://quiet-marigold-8dfaf3.netlify.app/"
                    target="_blank"
                    className="main-button"
                  >
                    Website Link
                  </Button>
                  {/* <Button
                    href="https://github.com/dnriski/webdasar"
                    className="main-button"
                    target="_blank"
                  >
                    Link Github
                  </Button> */}
                </Card.Body>
              </Card>
            </div>
            {/* Lukisan */}
            {/* Game */}
            <div className="div-container" data-aos="fade-down">
              <Carousel
                variant="dark"
                interval={1000}
                prevIcon={null}
                nextIcon={null}
              >
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius"
                    src={game1}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius"
                    src={game2}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius"
                    src={game3}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>
              <Card style={{ border: "none" }}>
                <Card.Body>
                  <Card.Title>Movie & Game</Card.Title>
                  <Card.Text>
                    Project Team Front end menggunakan React js
                  </Card.Text>
                  <Button
                    href="https://ephemeral-marigold-8157cc.netlify.app/"
                    target="_blank"
                    className="main-button"
                  >
                    Website Link
                  </Button>
                  {/* <Button
                    href="https://gitlab.com/superbootcamp-2022-batch-2/final-project-reactjs-riski-prima"
                    className="main-button"
                    target="_blank"
                  >
                    Link Github
                  </Button> */}
                </Card.Body>
              </Card>
            </div>
            {/* Game */}
            {/* Ecommerce */}
            <div className="div-container" data-aos="fade-up">
              <Carousel
                variant="dark"
                interval={1000}
                prevIcon={null}
                nextIcon={null}
              >
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius border"
                    src={gadget1}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius border"
                    src={gadget2}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius border"
                    src={gadget3}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>
              <Card style={{ border: "none" }}>
                <Card.Body>
                  <Card.Title>E Commerce Gadgetout</Card.Title>
                  <Card.Text>
                    Project Individu Fullstack menggunakan React js dan Node js
                  </Card.Text>
                  <Button
                    href="https://playful-sawine-06c838.netlify.app/"
                    target="_blank"
                    className="main-button"
                  >
                    Website Link
                  </Button>
                  {/* <Button
                    href="https://backend-riskidn.vercel.app/api-docs/"
                    target="_blank"
                    className="main-button"
                  >
                    Link Backend
                  </Button> */}
                  {/* <Button
                    href="https://gitlab.com/riskidn/frontend-final-project-super-bootcamp-2022-riski-dwi-nugroho"
                    className="main-button"
                    target="_blank"
                  >
                    Link Gitlab FE
                  </Button> */}
                  {/* <Button
                    href="https://gitlab.com/riskidn/backend-final-project-super-bootcamp-2022-riski-dwi-nugroho"
                    className="main-button"
                    target="_blank"
                  >
                    Link Gitlab BE
                  </Button> */}
                </Card.Body>
              </Card>
            </div>
            {/* Ecommerce */}
            {/* Catering */}
            <div className="div-container" data-aos="fade-up">
              <Carousel
                variant="dark"
                interval={1000}
                prevIcon={null}
                nextIcon={null}
              >
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius border"
                    src={catering1}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius border"
                    src={catering2}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100 image-radius border"
                    src={catering3}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>
              <Card style={{ border: "none" }}>
                <Card.Body>
                  <Card.Title>E Commerce Catering </Card.Title>
                  <Card.Text>
                    Project Fullstack menggunakan React js dan Node js
                  </Card.Text>
                  <Button
                    href="https://clinquant-duckanoo-327e44.netlify.app/"
                    target="_blank"
                    className="main-button"
                  >
                    Website Link
                  </Button>
                  {/* <Button
                    href="https://backend-final-project-team.vercel.app/"
                    target="_blank"
                    className="main-button"
                  >
                    Link Backend
                  </Button> */}
                  {/* <Button
                    href="https://gitlab.com/riskidn/frontend_final_project_team"
                    className="main-button"
                    target="_blank"
                  >
                    Link Gitlab FE
                  </Button> */}
                  {/* <Button
                    href="https://gitlab.com/mykaharudin/backend_final_project_team"
                    className="main-button"
                    target="_blank"
                  >
                    Link Gitlab BE
                  </Button> */}
                </Card.Body>
              </Card>
            </div>
            {/* Catering */}
            <div className="div-container"></div>
          </div>
        </Container>
      </section>
      <section style={{ background: "#E1EDFB" }} id="contact">
        <Container
          style={{
            minHeight: "90vh",
            background: "#E1EDFB",
            textAlign: "center",
            paddingTop: "20vh",
          }}
        >
          <h5 data-aos="fade-down">Contact</h5>
          <h2 style={{ marginBottom: "10px" }} data-aos="fade-down">
            Contact Me
          </h2>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              flexWrap: "wrap",
              marginTop: "30px",
            }}
            data-aos="fade-up"
          >
            <form
              action="mailto:nugrohoriski159@gmail.com"
              method="post"
              enctype="text/plain"
              className="form "
            >
              <label className="label">Name:</label>
              <br></br>
              <input type="text" name="name" className="input"></input>
              <br></br>
              <label className="label">Email:</label>
              <br></br>
              <input type="email" name="mail" className="input"></input>
              <br></br>
              <label className="label">Message:</label>
              <br></br>
              <textarea
                type="text"
                name="message"
                size="50"
                className="input"
              ></textarea>
              <br></br>
              <br></br>
              <input
                type="submit"
                value="Send"
                className="main-button send"
              ></input>
              <input
                type="reset"
                value="Reset"
                className="home-button send"
              ></input>
            </form>
          </div>
        </Container>
      </section>
      <section style={{ background: "#061283" }}>
        <Container
          style={{
            height: "10vh",
            background: "#061283",
            textAlign: "center",
            color: "white",
            padding: "10px",
          }}
        >
          <b
            style={{
              textAlign: "center",
              margin: "auto",
              height: "10vh",
            }}
          >
            {" "}
            Riski Dwi Nugroho || 2022{" "}
          </b>
        </Container>
      </section>
    </>
  );
};
export default Body;
